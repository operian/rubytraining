require 'csv'


 if ARGV.size!=1
 	puts "expecting csv file name"
 else
 	puts "parsing <#{ARGV[0]}>"
 	is_header_row=true
 	total_per_user=Hash.new(0)
 	total_per_user_per_category=Hash.new{|h,k| h[k]=Hash.new(0) }
 	total_per_year_per_month=Hash.new{|h,k| h[k]=Hash.new(0) }
 	CSV.foreach (ARGV[0]) do |row|
 		if is_header_row
 			if row[0]!="name" or row[1]!="amount" or row[2]!="category" or row[3]!="date" or row[4]!="description"
 				puts "Invalid CSV file! Expecting the following header: name,amount,category,date,description"
 				exit
 			end
 			is_header_row=false
 			next
 		end
 		total_per_user[row[0]]+=row[1].to_f
 		total_per_user_per_category[row[0]][row[2]]+=row[1].to_f
 		expense_date=Date.parse(row[3],'%d.%m.%Y')
 		total_per_year_per_month[expense_date.year][expense_date.month]+=row[1].to_f
 	end

 	puts "\n\n"
 	puts "================================================================="
 	puts "Total per user"
 	puts "=================================================================" 	
 	total_per_user.each do |user, total_spent|
 		puts user + " -- " + total_spent.to_s
 	end

 	puts "\n\n"
 	puts "================================================================="
 	puts "Total per user per category"
 	puts "=================================================================" 	
 	total_per_user_per_category.each do |user, categories_spent|
 		sum=0
 		categories_spent.each {|category, spent| sum+=spent}
 		puts user + " -- " + sum.to_s
 		categories_spent.each do |category,total_spent|
 			puts "          "+category+" -- " + total_spent.to_s
 		end
 	end

 	puts "\n\n"
 	puts "================================================================="
 	puts "Total per user per category"
 	puts "=================================================================" 	
 	total_per_year_per_month.each do |year, months_spent|
 		sum=0
 		months_spent.each {|month, spent| sum+=spent}
 		puts year.to_s + " -- " + sum.to_s
 		months_spent.each do |month, total_spent|
 			puts "          " + month.to_s + " -- " + total_spent.to_s
 		end
 	end

 end